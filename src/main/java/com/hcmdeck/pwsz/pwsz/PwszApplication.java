package com.hcmdeck.pwsz.pwsz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PwszApplication {

	public static void main(String[] args) {
		SpringApplication.run(PwszApplication.class, args);
	}

}
